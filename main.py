#!/usr/bin/python

import sys, time
from learn.daemon import Daemon
import BaseHTTPServer

class Handler(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_GET(self):
        self.wfile.write("Response")
        self.send_response(200)
        self.end_headers()

class MyDaemon(Daemon):
        def run(self):
            server_address = ('', 9000)
            httpd = BaseHTTPServer.HTTPServer(server_address, Handler)
            try:
                httpd.serve_forever()
            except Exception as e:
                sys.exit(0)

if __name__ == "__main__":
        daemon = MyDaemon('/tmp/daemon-example.pid')
        if len(sys.argv) == 2:
                if 'start' == sys.argv[1]:
                        daemon.start()
                elif 'stop' == sys.argv[1]:
                        daemon.stop()
                elif 'restart' == sys.argv[1]:
                        daemon.restart()
                else:
                        print "Unknown command"
                        sys.exit(2)
                sys.exit(0)
        else:
                print "usage: %s start|stop|restart" % sys.argv[0]
                sys.exit(2)
